The Employee Management portal currently provides the below functionalities:

1. Addition of a new employee
2. View list of all employees
3. View pay-slips generated for an employee
4. Create new pay-slip for an employee each month.

Technology and tools used:

1. Visual Studio 2013.
2. Entity Framework 5.x
3. MVC 5
4. Database-first approach
5. Local SQL DB

There are 2 tables in the local database:

1. Employee- This stores the employee data like First Name, Last Name, Annual Income, Super Annuation etc.
2. Payslip- This table stores the data related to payslip of an employee like Gross Income, Income Tax, Net Income, Payslip start date, Payslip End date

When an employee is added, by default the payslip for 1st month is generated in the background. The next payslips can be generated as required.

To run the project, open the solution in Visual Studio 2013 or later and run the application. If any other page is opened, please type in the below URL:

~localhost~/Employee

