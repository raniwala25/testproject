﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model= EmployeeManagement.Models;
using EmployeeManagement.Business;

namespace EmployeeManagement.Controllers
{
    public class EmployeeController : Controller
    {
        //
        // GET: /Employee/
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult AddEmployee()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddEmployee(Model.Employee emp)
        {
            EmployeeBL employeeBusiness = new EmployeeBL();
            employeeBusiness.AddEmployee(emp);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult ShowAllEmployees()
        {
            EmployeeBL employeeBusiness = new EmployeeBL();
            List<Model.Employee> empList= employeeBusiness.GetAllEmployees();
            return View(empList);
        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            EmployeeBL employeeBusiness = new EmployeeBL();
            Model.Employee emp = employeeBusiness.GetEmployeeDetails(id);
            return View(emp);
        }
        
        [HttpGet]
        public ActionResult ShowPaySlips(int id)
        {
            PaySlipBL payslipBL = new PaySlipBL();
            List<Model.PaySlip> paySlips = payslipBL.ShowAllPaySlipsForEmployee(id);
            return View(paySlips);
        }

        [HttpGet]
        public ActionResult ProcessNextPay(int id)
        {
            EmployeeBL employeeBl = new EmployeeBL();
            employeeBl.GenarateNextPayslip(id);

            return RedirectToAction("ShowAllEmployees");
        }
	}
}