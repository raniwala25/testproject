﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace EmployeeManagement.Models
{
    public class Employee
    {
        
        public int Id { get; set; }

        [Required(ErrorMessage="First Name is required for the Employee")]
        public string FirstName { get; set; }
        
        public string LastName { get; set; }

        [Required(ErrorMessage="Annual Salary is required for the Employee")]
        [RegularExpression(@"^[0-9]{0,10}$", ErrorMessage = "Annual Salary can only be in natural numbers")]
        public int AnnualSalary { get; set; }

        [Required(ErrorMessage="Super Annuation Rate % is required for the Employee")]
        [Range(0, 12, ErrorMessage="Super annuation rate can only be between 0-12%")]
        public int SuperAnnuation { get; set; }

        [Required(ErrorMessage="Payment start date is required for the Employee")]
        [Display(Name="Pay start date")]
        public DateTime PaymentStartDate { get; set; }

        [Display(Name="Date till pay processed")]
        public DateTime? LastSalaryDrawnUntil { get; set; }


    }
}