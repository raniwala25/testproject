﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace EmployeeManagement.Models
{
    public class PaySlip
    {

        public int Id { get; set; }

        public int EmployeeID { get; set; }

        public System.DateTime PayStartDate { get; set; }
        
        public System.DateTime PayEndDate { get; set; }

        public int GrossIncome { get; set; }

        public int IncomeTax { get; set; }
        
        public int NetIncome { get; set; }
        
        public int SuperAnnuation { get; set; }

        //public virtual Employee Employee { get; set; }


    }
}