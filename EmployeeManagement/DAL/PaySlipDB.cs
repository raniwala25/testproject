﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Model = EmployeeManagement.Models;


namespace EmployeeManagement.DAL
{
    public class PaySlipDB
    {
        EmployeeManagementEntities dbContext = null;
        public int AddPaySlip(Model.PaySlip paySlip)
        {
            int paySlipId = 0;
            PaySlip dbPaySlip = new PaySlip();
            dbPaySlip.EmployeeID = paySlip.EmployeeID;
            dbPaySlip.GrossIncome = paySlip.GrossIncome;
            dbPaySlip.IncomeTax = paySlip.IncomeTax;
            dbPaySlip.NetIncome = paySlip.NetIncome;
            dbPaySlip.PayEndDate = paySlip.PayEndDate;
            dbPaySlip.PayStartDate = paySlip.PayStartDate;
            dbPaySlip.SuperAnnuation = paySlip.SuperAnnuation;

            dbContext = new EmployeeManagementEntities();
            try
            {
                dbContext.PaySlips.Add(dbPaySlip);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbContext.SaveChanges();
                dbContext.Dispose();
                paySlipId = dbPaySlip.Id;
            }
            return paySlipId;
        }

        public List<Model.PaySlip> ShowAllPayslips(int empId)
        {
            List<Model.PaySlip> payslipList = null;

            dbContext = new EmployeeManagementEntities();

            payslipList = (from paySlip in dbContext.PaySlips
                           where paySlip.EmployeeID == empId
                           select new Model.PaySlip()
                           {
                               Id = paySlip.Id,
                               GrossIncome = paySlip.GrossIncome,
                               EmployeeID = paySlip.EmployeeID,
                               IncomeTax = (int) paySlip.IncomeTax,
                               NetIncome = paySlip.NetIncome,
                               PayEndDate = paySlip.PayEndDate,
                               PayStartDate = paySlip.PayStartDate,
                               SuperAnnuation = paySlip.SuperAnnuation
                           }).ToList();
            return payslipList;
        }
    }
}