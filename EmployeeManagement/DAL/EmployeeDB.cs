﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Model= EmployeeManagement.Models;

namespace EmployeeManagement.DAL
{
    public class EmployeeDB
    {
        EmployeeManagementEntities employeeDBContext;
        public int AddEmployee(Model.Employee emp)
        {
            int addedEmployeeId = 0;

            Employee empToBeAdded = new Employee();
            empToBeAdded.FirstName = emp.FirstName;
            empToBeAdded.AnnualSalary = emp.AnnualSalary;
            empToBeAdded.LastName = emp.LastName;
            empToBeAdded.SalaryStartDate = emp.PaymentStartDate;
            empToBeAdded.SuperRate = emp.SuperAnnuation;

            employeeDBContext = new  EmployeeManagementEntities();
            try
            {
                employeeDBContext.Employees.Add(empToBeAdded);
            }
            catch (Exception ex)
            {
                addedEmployeeId = 0;
            }
            finally
            {
                employeeDBContext.SaveChanges();
                employeeDBContext.Dispose();
                addedEmployeeId = empToBeAdded.Id;
            }

            return addedEmployeeId;
        }

        public Model.Employee GetEmployeeDetails(int empId)
        {

            employeeDBContext = new EmployeeManagementEntities();

            Model.Employee empRec = (from emp in employeeDBContext.Employees
                                     where emp.Id == empId
                                     select new Model.Employee()
                                     {
                                         Id = emp.Id,
                                         FirstName = emp.FirstName,
                                         LastName = emp.LastName,
                                         AnnualSalary = emp.AnnualSalary,
                                         LastSalaryDrawnUntil = emp.LastSalDrawnOn,
                                         PaymentStartDate = emp.SalaryStartDate,
                                         SuperAnnuation = emp.SuperRate
                                     }).FirstOrDefault();
            return empRec;


        }

        public bool UpdateLastDrawnSalary(int empId, DateTime LastSalaryDrawnOn)
        {
            bool IsUpdated = false;
            employeeDBContext = new EmployeeManagementEntities();
            var dbEmp = (from emp in employeeDBContext.Employees
                         where emp.Id == empId
                         select emp).FirstOrDefault();
            dbEmp.LastSalDrawnOn = LastSalaryDrawnOn;
            try
            {
                employeeDBContext.SaveChanges();
                IsUpdated = true;
            }
            catch
            {

            }
            finally
            {
                employeeDBContext.Dispose();
            }
            return IsUpdated;
        }

        public List<Model.Employee> GetAllEmployees()
        {
            List<Model.Employee> employeeList = null;

            employeeDBContext = new EmployeeManagementEntities();

            employeeList = (from emp in employeeDBContext.Employees
                            select new Model.Employee()
                            {
                                Id = emp.Id,
                                FirstName = emp.FirstName,
                                LastName = emp.LastName,
                                LastSalaryDrawnUntil = emp.LastSalDrawnOn,
                                AnnualSalary = emp.AnnualSalary,
                                SuperAnnuation = emp.SuperRate,
                                PaymentStartDate = emp.SalaryStartDate
                            }).ToList(); ;

            return employeeList;
        }
    }
}