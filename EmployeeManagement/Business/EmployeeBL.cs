﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Model = EmployeeManagement.Models;
using EmployeeManagement.DAL;

namespace EmployeeManagement.Business
{
    public class EmployeeBL
    {
        EmployeeDB employeeDBContext;
        public int AddEmployee(Model.Employee emp)
        {
            int addedEmpId = 0;
            employeeDBContext = new EmployeeDB();
            addedEmpId = employeeDBContext.AddEmployee(emp);

            if (addedEmpId != 0)
            {
                Model.PaySlip paySlip = GeneratePayslipForEmployee(addedEmpId);

                PaySlipBL payslipBL = new PaySlipBL();
                int payslipId = payslipBL.AddPaySlip(paySlip);
                if (payslipId > 0)
                {
                    employeeDBContext.UpdateLastDrawnSalary(addedEmpId, paySlip.PayEndDate);
                }
            }

            return addedEmpId;
        }

        public Model.Employee GetEmployeeDetails(int id)
        {
            employeeDBContext = new EmployeeDB();
            return employeeDBContext.GetEmployeeDetails(id);
        }
        public List<Model.Employee> GetAllEmployees()
        {
            employeeDBContext = new EmployeeDB();
            return employeeDBContext.GetAllEmployees();
        }
        public void GenarateNextPayslip(int empId)
        {
            PaySlipBL payslipBL = new PaySlipBL();
            Model.PaySlip paySlip = GeneratePayslipForEmployee(empId);
            int payId= payslipBL.AddPaySlip(paySlip);
            if(payId!=0)
            {
                employeeDBContext = new EmployeeDB();
                employeeDBContext.UpdateLastDrawnSalary(empId, paySlip.PayEndDate);
            }
            
        }
        private Model.PaySlip GeneratePayslipForEmployee(int empId)
        {
            double grossIncome = 0;
            Model.PaySlip paySlip = new Model.PaySlip();

            employeeDBContext = new EmployeeDB();
            Model.Employee empRecord = employeeDBContext.GetEmployeeDetails(empId);
            if (empRecord.LastSalaryDrawnUntil == null || empRecord.LastSalaryDrawnUntil.Equals(DateTime.MinValue))
            {
                paySlip.PayStartDate = empRecord.PaymentStartDate;
                paySlip.PayEndDate = new DateTime(empRecord.PaymentStartDate.Year, empRecord.PaymentStartDate.Month, DateTime.DaysInMonth(empRecord.PaymentStartDate.Year,
                    empRecord.PaymentStartDate.Month));
            }
            else
            {
                paySlip.PayStartDate = Convert.ToDateTime(empRecord.LastSalaryDrawnUntil).AddDays(1);
                paySlip.PayEndDate = new DateTime(paySlip.PayStartDate.Year, paySlip.PayStartDate.Month, DateTime.DaysInMonth(paySlip.PayStartDate.Year,
                    paySlip.PayStartDate.Month));
            }
            grossIncome = Convert.ToDouble(empRecord.AnnualSalary / 12);
            paySlip.EmployeeID = empId;
            paySlip.GrossIncome = Convert.ToInt32(Math.Round(grossIncome));
            paySlip.IncomeTax = Convert.ToInt32 (CalculateIncomeTax(empRecord.AnnualSalary));
            paySlip.NetIncome = Convert.ToInt32((int)grossIncome - (int)paySlip.IncomeTax);
            double superAnnuation = grossIncome * empRecord.SuperAnnuation;
            superAnnuation = superAnnuation / 100;
            superAnnuation = Math.Round(superAnnuation);
            paySlip.SuperAnnuation = Convert.ToInt32(superAnnuation);

            return paySlip;
        }
        private decimal CalculateIncomeTax(decimal grossIncome)
        {
            float grossincome = Convert.ToInt32(grossIncome);
            decimal incomeTax = 0;
            if (grossIncome <= 18200)
            {
                return 0;
            }
            else if (grossIncome <= 37000)
            {
                incomeTax = (decimal)Math.Round(.19 * (grossincome - 18200));
            }
            else if (grossIncome <= 87000)
            {
                incomeTax = (decimal)Math.Round(.325 * (grossincome - 37000)) + 3572;
            }
            else if (grossIncome <= 180000)
            {
                incomeTax = (decimal)Math.Round(.37 * (grossincome - 87000)) + 19822;
            }
            else
            {
                incomeTax = (decimal)Math.Round(.45 * (grossincome - 180000)) + 54232;
            }
            return incomeTax/12;
        }
    }
}