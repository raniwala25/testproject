﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Model = EmployeeManagement.Models;
using EmployeeManagement.DAL;

namespace EmployeeManagement.Business
{
    public class PaySlipBL
    {
        PaySlipDB dbContext = null;
        public int AddPaySlip(Model.PaySlip paySlip)
        {
            dbContext = new PaySlipDB();
            return dbContext.AddPaySlip(paySlip);

        }

        public List<Model.PaySlip> ShowAllPaySlipsForEmployee(int empId)
        {
            dbContext = new PaySlipDB();
            return dbContext.ShowAllPayslips(empId);
        }
    }
}